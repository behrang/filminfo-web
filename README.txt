
Installation ======================

--	clone project from git

	git clone git@gitlab.com:behrang/filminfo-web.git
   	Or 
   	git clone https://gitlab.com/behrang/filminfo-web.git
   	
--	create database

	create database and import dump data from db-dump folder in the root of project

--	setup database connection

	copy or rename wp-config-local.php to wp-config.php and change database connection information as your own


